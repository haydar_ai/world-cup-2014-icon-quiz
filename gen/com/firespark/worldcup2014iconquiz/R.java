/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.firespark.worldcup2014iconquiz;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int l1_1=0x7f020001;
        public static final int l1_2=0x7f020002;
        public static final int l1_3=0x7f020003;
        public static final int l1_4=0x7f020004;
        public static final int l1_5=0x7f020005;
        public static final int l1_6=0x7f020006;
        public static final int l1_7=0x7f020007;
        public static final int l1_8=0x7f020008;
        public static final int l1_9=0x7f020009;
        public static final int l2_1=0x7f02000a;
        public static final int l2_2=0x7f02000b;
        public static final int l2_3=0x7f02000c;
        public static final int l2_4=0x7f02000d;
        public static final int l2_5=0x7f02000e;
        public static final int l2_6=0x7f02000f;
        public static final int l2_7=0x7f020010;
        public static final int l2_8=0x7f020011;
        public static final int l2_9=0x7f020012;
        public static final int l3_1=0x7f020013;
        public static final int l3_2=0x7f020014;
        public static final int l3_3=0x7f020015;
        public static final int l3_4=0x7f020016;
        public static final int l3_5=0x7f020017;
        public static final int l3_6=0x7f020018;
        public static final int l3_7=0x7f020019;
        public static final int l3_8=0x7f02001a;
        public static final int l3_9=0x7f02001b;
    }
    public static final class id {
        public static final int action_about=0x7f07000f;
        public static final int action_clearscore=0x7f070010;
        public static final int activityAbout=0x7f070000;
        public static final int buttonCheck=0x7f070004;
        public static final int edittextAnswer=0x7f070003;
        public static final int feedbackButton=0x7f070001;
        public static final int gridviewLevel=0x7f070005;
        public static final int highscoreButton=0x7f070007;
        public static final int imageviewQuestion=0x7f070002;
        public static final int level1Button=0x7f070009;
        public static final int level2Button=0x7f07000a;
        public static final int level3Button=0x7f07000b;
        public static final int playButton=0x7f070006;
        public static final int tutorialButton=0x7f070008;
        public static final int tutorialact=0x7f07000e;
        public static final int tvSolved=0x7f07000c;
        public static final int tvUnsolved=0x7f07000d;
    }
    public static final class layout {
        public static final int activity_about=0x7f030000;
        public static final int activity_detail=0x7f030001;
        public static final int activity_level=0x7f030002;
        public static final int activity_main=0x7f030003;
        public static final int activity_play=0x7f030004;
        public static final int activity_score=0x7f030005;
        public static final int activity_tutorial=0x7f030006;
    }
    public static final class menu {
        public static final int main=0x7f060000;
        public static final int score=0x7f060001;
    }
    public static final class string {
        public static final int action_about=0x7f040002;
        public static final int action_clearscore=0x7f040003;
        public static final int app_name=0x7f040000;
        public static final int hello_world=0x7f040001;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f050000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050001;
    }
}
