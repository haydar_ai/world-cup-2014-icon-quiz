package com.firespark.worldcup2014iconquiz;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.firespark.worldcup2014iconquiz.adapter.ImageAdapter;
import com.firespark.worldcup2014iconquiz.helper.StorageHelper;

public class DetailActivity extends Activity {

	private String answer;
	private String userAnswer;
	private String[] answers;
	private String[] answers1 = {};
	private String[] answers2 = {};
	private String[] answers3 = {};
	private Integer[] solveds;
	private Integer[] solved1 = {};
	private Integer[] solved2 = {};
	private Integer[] solved3 = {};
	private static final String sharedPreference = "first";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		solved1 = new Integer[9];
		solved2 = new Integer[9];
		solved3 = new Integer[9];
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		if (!settings.contains(sharedPreference)) {
			Arrays.fill(solved1, Integer.valueOf(0));
			Arrays.fill(solved2, Integer.valueOf(0));
			Arrays.fill(solved3, Integer.valueOf(0));
			StorageHelper.saveArray(solved1, "worldcup2014iconquizsolved1");
			StorageHelper.saveArray(solved2, "worldcup2014iconquizsolved2");
			StorageHelper.saveArray(solved3, "worldcup2014iconquizsolved3");
			Editor editor = settings.edit();
			editor.putBoolean(sharedPreference, false);
			editor.commit();
			Log.d("solvedLength", String.valueOf(solved1.length));
		} else {
			solved1 = StorageHelper.loadArray("worldcup2014iconquizsolved1");
			solved2 = StorageHelper.loadArray("worldcup2014iconquizsolved2");
			solved3 = StorageHelper.loadArray("worldcup2014iconquizsolved3");
		}
		answers = new String[9];
		answers1 = new String[] { "Neymar", "Etoo", "Xavi", "Alexis", "Falcao",
				"Suarez", "Ribery", "Dzeko", "Cristiano" };
		answers2 = new String[] { "Ecuador", "Uruguay", "Spain", "Brazil",
				"Chile", "Cameroon", "Belgium", "Croatia", "France" };
		answers3 = new String[] { "Modric", "Chicarito", "Van Persie",
				"Cahill", "Drogba", "Balotelli", "Messi", "Ozil", "Hazard" };
		Intent i = getIntent();
		Bundle b = i.getExtras();
		final int level = b.getInt("level");
		final int position = b.getInt("id");
		switch (level) {
		case 1:
			answers = answers1;
			solveds = solved1;
			break;
		case 2:
			answers = answers2;
			solveds = solved2;
			break;
		case 3:
			answers = answers3;
			solveds = solved3;
			break;
		default:
			break;
		}

		setTitle("Level " + level + " - " + (position + 1));
		answer = answers[position];
		ImageAdapter imageAdapter = new ImageAdapter(this, level);
		ImageView imageView = (ImageView) findViewById(R.id.imageviewQuestion);
		imageView.setImageResource(imageAdapter.mThumbIds[position]);
		final EditText editText = (EditText) findViewById(R.id.edittextAnswer);
		if (solveds[position] != 0) {
			editText.setEnabled(false);
			editText.setText(answer);
		}
		final Button submitButton = (Button) findViewById(R.id.buttonCheck);
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				answer = answer.toLowerCase();
				userAnswer = editText.getText().toString();
				userAnswer = userAnswer.toLowerCase();
				Log.d("answer", answer);
				Log.d("userAnswer", userAnswer);
				if (answer.equals(userAnswer)) {
					Toast.makeText(getApplicationContext(), "True!",
							Toast.LENGTH_SHORT).show();
					editText.setEnabled(false);
					submitButton.setEnabled(false);
					switch (level) {
					case 1:
						solved1[position] = 1;
						StorageHelper.saveArray(solved1,
								"worldcup2014iconquizsolved1");
						break;
					case 2:
						solved2[position] = 1;
						StorageHelper.saveArray(solved2,
								"worldcup2014iconquizsolved2");
						break;
					case 3:
						solved3[position] = 1;
						StorageHelper.saveArray(solved3,
								"worldcup2014iconquizsolved3");
						break;
					default:
						break;
					}
				} else {
					Toast.makeText(getApplicationContext(), "False, try again",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}
