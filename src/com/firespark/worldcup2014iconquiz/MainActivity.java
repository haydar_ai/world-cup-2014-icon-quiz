package com.firespark.worldcup2014iconquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Button initializer
		Button playButton = (Button)findViewById(R.id.playButton);
		Button highscoreButton = (Button)findViewById(R.id.highscoreButton);
		Button tutorialButton = (Button)findViewById(R.id.tutorialButton);
		
		// Button onClick listener
		playButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, PlayActivity.class);
				startActivity(i);
			}
		});
		highscoreButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, ScoreActivity.class);
				startActivity(i);
			}
		});
		tutorialButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, HelpActivity.class);
				startActivity(i);
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_about) {
			Intent i = new Intent(getApplicationContext(), AboutActivity.class);
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
	}
}
