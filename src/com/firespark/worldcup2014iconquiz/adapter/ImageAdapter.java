package com.firespark.worldcup2014iconquiz.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.firespark.worldcup2014iconquiz.R;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	public Integer[] mThumbIds;

	// Image Assets
	private Integer[] mLevel1ThumbIds = { R.drawable.l1_1, R.drawable.l1_2,
			R.drawable.l1_3, R.drawable.l1_4, R.drawable.l1_5, R.drawable.l1_6,
			R.drawable.l1_7, R.drawable.l1_8, R.drawable.l1_9 };
	private Integer[] mLevel2ThumbIds = { R.drawable.l2_1, R.drawable.l2_2,
			R.drawable.l2_3, R.drawable.l2_4, R.drawable.l2_5, R.drawable.l2_6,
			R.drawable.l2_7, R.drawable.l2_8, R.drawable.l2_9 };
	private Integer[] mLevel3ThumbIds = { R.drawable.l3_1, R.drawable.l3_2,
			R.drawable.l3_3, R.drawable.l3_4, R.drawable.l3_5, R.drawable.l3_6,
			R.drawable.l3_7, R.drawable.l3_8, R.drawable.l3_9 };

	// Constructor
	public ImageAdapter(Context c, int i) {
		mContext = c;
		switch (i) {
		case 1:
			this.mThumbIds = mLevel1ThumbIds;
			break;
		case 2:
			this.mThumbIds = mLevel2ThumbIds;
			break;
		case 3:
			this.mThumbIds = mLevel3ThumbIds;
			break;
		default:
			break;
		}
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
			imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
			imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(mThumbIds[position]);
		return imageView;
	}

}