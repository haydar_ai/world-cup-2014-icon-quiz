package com.firespark.worldcup2014iconquiz;

import java.util.Arrays;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.firespark.worldcup2014iconquiz.helper.StorageHelper;

public class ScoreActivity extends Activity {
	private static final String sharedPreference = "first";
	private Integer totalSolved;
	private Integer totalUnsolved;
	private int total;
	private Integer[] solved1;
	private Integer[] solved2;
	private Integer[] solved3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("Highscore");
		solved1 = new Integer[9];
		solved2 = new Integer[9];
		solved3 = new Integer[9];
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		if (!settings.contains(sharedPreference)) {
			Arrays.fill(solved1, Integer.valueOf(0));
			Arrays.fill(solved2, Integer.valueOf(0));
			Arrays.fill(solved3, Integer.valueOf(0));
			StorageHelper.saveArray(solved1, "worldcup2014iconquizsolved1");
			StorageHelper.saveArray(solved2, "worldcup2014iconquizsolved2");
			StorageHelper.saveArray(solved3, "worldcup2014iconquizsolved3");
			Editor editor = settings.edit();
			editor.putBoolean(sharedPreference, false);
			editor.commit();
		} else {
			solved1 = StorageHelper.loadArray("worldcup2014iconquizsolved1");
			solved2 = StorageHelper.loadArray("worldcup2014iconquizsolved2");
			solved3 = StorageHelper.loadArray("worldcup2014iconquizsolved3");
		}
		CalculateScore();
	}

	private void CalculateScore() {
		total = solved1.length + solved2.length + solved3.length;
		totalSolved = 0;
		for (int i = 0; i < solved1.length; i++) {
			totalSolved += solved1[i];
		}
		for (int i = 0; i < solved2.length; i++) {
			totalSolved += solved2[i];
		}
		for (int i = 0; i < solved3.length; i++) {
			totalSolved += solved3[i];
		}
		totalUnsolved = total - totalSolved;
		TextView tvSolved = (TextView) findViewById(R.id.tvSolved);
		tvSolved.setText(String.valueOf(totalSolved));
		TextView tvUnsolved = (TextView) findViewById(R.id.tvUnsolved);
		tvUnsolved.setText(String.valueOf(totalUnsolved));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.score, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_clearscore) {
			Arrays.fill(solved1, Integer.valueOf(0));
			Arrays.fill(solved2, Integer.valueOf(0));
			Arrays.fill(solved3, Integer.valueOf(0));
			StorageHelper.saveArray(solved1, "worldcup2014iconquizsolved1");
			StorageHelper.saveArray(solved2, "worldcup2014iconquizsolved2");
			StorageHelper.saveArray(solved3, "worldcup2014iconquizsolved3");
			CalculateScore();
			Toast.makeText(getApplicationContext(), "Highscore resetted",
					Toast.LENGTH_SHORT).show();
		}
		return super.onOptionsItemSelected(item);
	}
}
