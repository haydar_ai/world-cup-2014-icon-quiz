package com.firespark.worldcup2014iconquiz.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.os.Environment;

public class StorageHelper {
	public static void saveArray(Integer[] list, String name) {
		File filedir = Environment.getExternalStorageDirectory();
		File filename = new File(filedir, name);
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(list);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Integer[] loadArray(String name) {
		try {
			File filedir = Environment.getExternalStorageDirectory();
			File filename = new File(filedir, name);
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Integer[] list = (Integer[]) ois.readObject();
			ois.close();
			return list;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Integer[] empty = new Integer[] { 0 };
		return empty;
	}
}
