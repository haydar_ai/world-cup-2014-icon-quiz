package com.firespark.worldcup2014iconquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.firespark.worldcup2014iconquiz.adapter.ImageAdapter;

public class LevelActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_level);
		Intent i = getIntent();
		final int level = i.getIntExtra("level", 1);
		setTitle("Level " + String.valueOf(level));
		GridView gridviewLevel = (GridView)findViewById(R.id.gridviewLevel);
		gridviewLevel.setAdapter(new ImageAdapter(this, level));
		
		gridviewLevel.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Intent i = new Intent(getApplicationContext(), DetailActivity.class);
				Bundle b = new Bundle();
				b.putInt("level", level);
				b.putInt("id", position);
				i.putExtras(b);
				startActivity(i);
			}
		});
	}
}
