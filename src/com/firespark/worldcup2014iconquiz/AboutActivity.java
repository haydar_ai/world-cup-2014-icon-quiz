package com.firespark.worldcup2014iconquiz;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		Button feedbackButton = (Button)findViewById(R.id.feedbackButton);
		feedbackButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, "haydar_ai@yahoo.com");
				i.putExtra(Intent.EXTRA_SUBJECT, "[World Cup 2014 Icon Quiz] Feedback");
				try {
					startActivity(Intent.createChooser(i, "Send email"));
				} catch (ActivityNotFoundException e) {
					Toast.makeText(getApplicationContext(), "No email client installed", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}