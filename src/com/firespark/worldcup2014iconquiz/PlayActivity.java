package com.firespark.worldcup2014iconquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlayActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("Levels");
		
		// Button initializer
		Button level1Button = (Button)findViewById(R.id.level1Button);
		Button level2Button = (Button)findViewById(R.id.level2Button);
		Button level3Button = (Button)findViewById(R.id.level3Button);
		
		// Button onClickListener
		level1Button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PlayActivity.this, LevelActivity.class);
				i.putExtra("level", 1);
				startActivity(i);
			}
		});
		level2Button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PlayActivity.this, LevelActivity.class);
				i.putExtra("level", 2);
				startActivity(i);
			}
		});
		level3Button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PlayActivity.this, LevelActivity.class);
				i.putExtra("level", 3);
				startActivity(i);
			}
		});
	}
}
